package py.com.pol.fpuna.paronline.client;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

public class CategoryRestClient {

    public List<py.com.pol.fpuna.commons.model.response.Category> getAll() {
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/category/list");
        Response response = target.request().get();
        List<py.com.pol.fpuna.commons.model.response.Category> categories = response.readEntity(
            new GenericType<List<py.com.pol.fpuna.commons.model.response.Category>>() {}
        );
        response.close();
        return categories;
    }

    public py.com.pol.fpuna.commons.model.response.Category getById(Integer id) {
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/category/" + id);
        Response response = target.request().get();
        py.com.pol.fpuna.commons.model.response.Category values = response.readEntity(py.com.pol.fpuna.commons.model.response.Category.class);
        response.close();
        return values; 
    }
    
    public boolean update(py.com.pol.fpuna.commons.model.request.Category entity) {
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/category");
        Entity<py.com.pol.fpuna.commons.model.request.Category> someEntity = Entity.entity(entity, MediaType.APPLICATION_JSON);
        Response response = target.request().post(someEntity);
        response.close();
        return true;
    }    

    public boolean add(py.com.pol.fpuna.commons.model.request.Category entity) {
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/category");
        Entity<py.com.pol.fpuna.commons.model.request.Category> someEntity = Entity.entity(entity, MediaType.APPLICATION_JSON);
        Response response = target.request().post(someEntity);
        response.close();
        return true;
    }
    
    public boolean delete(Integer id){
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/category/" + id);
        Response response = target.request().delete();
        response.close();
        return true;        
    }
    
}
