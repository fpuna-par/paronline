//package py.com.pol.fpuna.paronline.mock;
//
//import java.util.Arrays;
//import py.com.pol.fpuna.commons.entity.domain.CategoryResponse;
//import py.com.pol.fpuna.commons.entity.domain.CategoryResponseData;
//import py.com.pol.fpuna.commons.entity.domain.ProductResponse;
//import py.com.pol.fpuna.commons.entity.domain.ProductResponseData;
//
///**
// *
// * @author Gino Junchaya
// */
//public class MockUtil {
//    
//    public static ProductResponseData mockProducts(){
//        ProductResponseData data = new ProductResponseData();
//        data.setData(Arrays.asList(mockProduct(), mockProduct()));
//        return data;
//    }
//    
//    public static CategoryResponseData mockCategories(){
//        CategoryResponseData data = new CategoryResponseData();
//        data.setData(Arrays.asList(mockCategory(), mockCategory()));
//        return data;
//    }
//    
//    public static ProductResponse mockProduct(){
//        ProductResponse product = new ProductResponse();
//        product.setId(1);
//        product.setName("Lavarropa");
//        product.setStatus("S");
//        product.setCategory(mockCategory());
//        return product;
//    }
//    
//    public static CategoryResponse mockCategory(){
//        CategoryResponse category = new CategoryResponse();
//        category.setId(1);
//        category.setName("Electrodomésticos");
//        return category;
//    }
//    
//}
