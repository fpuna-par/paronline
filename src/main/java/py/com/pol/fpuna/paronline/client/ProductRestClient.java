package py.com.pol.fpuna.paronline.client;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import py.com.pol.fpuna.commons.entity.Product;

public class ProductRestClient {

    public List<Product> getAll() {
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/product/list");
        Response response = target.request().get();
        List<Product> products = response.readEntity(
            new GenericType<List<Product>>() {}
        );
        response.close();
        return products;  
    }

    public Product getById(Integer id) {
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/product/" + id);
        Response response = target.request().get();
        Product values = response.readEntity(Product.class);
        response.close();
        return values; 
    }
    
    public boolean update(py.com.pol.fpuna.commons.entity.Product entity) {
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/product");
        Entity<py.com.pol.fpuna.commons.entity.Product> someEntity = Entity.entity(entity, MediaType.APPLICATION_JSON);
        Response response = target.request().post(someEntity);
        response.close();
        return true;
    }

    public boolean add(py.com.pol.fpuna.commons.entity.Product entity) {
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/product");
        Entity<py.com.pol.fpuna.commons.entity.Product> someEntity = Entity.entity(entity, MediaType.APPLICATION_JSON);
        Response response = target.request().post(someEntity);
        response.close();
        return true;
    }
    
    public boolean delete(Integer id){
        Client client = ResteasyClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/parprdmcs/api/product/" + id);
        Response response = target.request().delete();
        response.close();
        return true;        
    }
    
}
