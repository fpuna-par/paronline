package py.com.pol.fpuna.paronline.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.pol.fpuna.commons.entity.Product;
import py.com.pol.fpuna.paronline.client.CategoryRestClient;
import py.com.pol.fpuna.paronline.client.ProductRestClient;

@WebServlet(name = "HomeServlet", urlPatterns = "/home")
public class HomeController extends HttpServlet { 
    
    final ProductRestClient productRestClient = new ProductRestClient();
    final CategoryRestClient categoryRestClient = new CategoryRestClient();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/home/home.jsp");
        List<Product> productos = productRestClient.getAll();
        List<py.com.pol.fpuna.commons.model.response.Category> categories = categoryRestClient.getAll();
        request.setAttribute("page", "productos");
        request.setAttribute("categorias", categories);
        request.setAttribute("productos", productos);
        dispatcher.forward(request, response);
    }
    
}
