<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style><%@include file="/WEB-INF/cart/styles.css"%></style>
        <script><%@include file="/WEB-INF/cart/scripts.js"%></script>
    </head>
    <body>
        <section class="cart-box"> 
            <h3>Artículos en el carrito</h3> 
            <section class="cart-login"> 
                <section id="items" class="kart-items"></section>
                <button class="btn-primary" style="width: auto!important; float: right" onClick="hiddenConfirmPurchase()" >Realizar compra</button>
            </section>
        </section>
    </body>
</html>
