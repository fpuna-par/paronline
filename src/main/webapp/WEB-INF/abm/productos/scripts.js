/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function showFormProduct(){
    cleanProductsFields();
    getAllCategorias();
    document.getElementById("btn-guardar-product").hidden = false;
    document.getElementById("btn-editar-product").hidden = true;
    document.getElementById("form-products-modal").hidden = false;
    document.getElementById("container-home").setAttribute("style", "opacity:0.4");
}

function cleanProductsFields(){
    document.getElementById("formProduct-descripcion").value = "";
    document.getElementById("formProduct-categoria").value = "";
    document.getElementById("formProduct-precioUnit").value = "";
    document.getElementById("formProduct-cantidad").value = "";
}

function getAllCategorias(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:8080/parprdmcs/api/category/list", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = xhr.responseText;
            response = JSON.parse(response);
            return loadCategoryInForm(response);
        }
        else{
            console.log(xhr.responseText);
            console.log("ERROR");            
            showNoData();
        }
    };
    xhr.send();    
}

function loadCategoryInForm(categorias){
    console.log(categorias);
    var selectCat = document.getElementById("formProduct-categoria");
    removeAllChilds(selectCat);
    for(var i = 0; i < categorias.length; i++){
        var option = document.createElement("option");
        option.setAttribute("value", JSON.stringify(categorias[i]));
        option.innerText = categorias[i].description;
        selectCat.appendChild(option);
    }
}

function closeFormProduct(){
    document.getElementById("form-products-modal").hidden = true;
    document.getElementById("container-home").setAttribute("style", "opacity:1");    
}

function saveProducto(){
    
    var object = validateProductFields();
    
    if(object === null || object === undefined){
        console.log("Campos invalidos");
        return;
    }
    
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/parprdmcs/api/product", false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 201) {
            closeFormProduct();
            showABMProductos();
            showSnackbar("Producto registrado correctamente", "success");
        }
        else{
            console.log(xhr.responseText);
            console.log("ERROR");
            showSnackbar("Ha ocurrido un error al procesar la solicitud", "error");            
        }
    };
    console.log(object);
    xhr.send(JSON.stringify(object));
}

function validateProductFields(){
    var descripcion = document.getElementById("formProduct-descripcion").value;
    var idCategoria = document.getElementById("formProduct-categoria").value;
    var precioUnit = document.getElementById("formProduct-precioUnit").value;
    var cantidad = document.getElementById("formProduct-cantidad").value;
    var object = {
        description: descripcion,
        categoryId: JSON.parse(idCategoria).categoryId,
        price: precioUnit,
        qty: cantidad,
        status: "A"
    };
    return object;
}

function deleteProduct(idProducto){
    console.log("Eliminar: ", idProducto);
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", "http://localhost:8080/parprdmcs/api/product/" + idProducto, false);
    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            showABMProductos();
            showSnackbar("Producto eliminado exitosamente", "success");                        
        }
        else{
            console.log(xhr.responseText);
            console.log("ERROR");
            showSnackbar("Ha ocurrido un error al procesar la solicitud", "error");                        
        }
    };
    xhr.send();
}

function editarProduct(producto){
    console.log("Editar: ", producto);
    document.getElementById("formProduct-descripcion").value = producto.description;
    document.getElementById("formProduct-categoria").value = producto.categoryId.id;
    document.getElementById("formProduct-precioUnit").value = producto.price;
    document.getElementById("formProduct-cantidad").value = producto.qty;
    document.getElementById("btn-guardar-product").hidden = true;
    document.getElementById("btn-editar-product").hidden = false;
    getAllCategorias();
    document.getElementById("form-products-modal").hidden = false;
    document.getElementById("container-home").setAttribute("style", "opacity:0.4");
    var btnEditar = document.getElementById("btn-editar-product-ok");
    btnEditar.setAttribute("onclick", "updateProduct(" + producto.productId + ")");
}

function updateProduct(producto){
    var object = validateProductFields();
    if(object === null || object === undefined){
        console.log("Campos invalidos");
        return;
    }
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", "http://localhost:8080/parprdmcs/api/product/" + producto, false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            closeFormProduct();
            showABMProductos();
            showSnackbar("Datos actualizados correctamente", "success");                        
        }
        else{
            console.log(xhr.responseText);
            console.log("ERROR");
            showSnackbar("Ha ocurrido un error al procesar la solicitud", "error");                        
        }
    };
    console.log(object);
    xhr.send(JSON.stringify(object));    
}