<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style><%@include file="/WEB-INF/login/styles.css"%></style>
        <script><%@include file="/WEB-INF/login/scripts.js"%></script>
        <script><%@include file="/WEB-INF/js-core/cookie.js"%></script>
    </head> 
    <body>
        <section class="login-box"> 
            <h3>Iniciar sesión</h3> 
            <section class="form-login"> 
                <input type="text" id="username" placeholder="Ingrese su nombre de usuario" autocomplete="off" onkeyup="if(event.keyCode == 13) login()"/>
                <input type="password" style="margin-bottom: 1em" id="password" placeholder="Ingrese su contraseña" onkeyup="if(event.keyCode == 13) login()"/>
                <h5 class="error-login" id="error-message" hidden></h5> 
                <button class="btn-primary" onclick="login()">Ingresar</button>
                <button class="btn-secondary-login" onclick="continueGuest()">Continuar como invitado</button>
            </section>
        </section>      
    </body>
</html>
