<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style><%@include file="/WEB-INF/modals/snackbar/styles.css"%></style>
    </head>
    <body>
        <section class="modal-snackbar-container">
            <section id="modal-snackbar-header" class="modal-snackbar-header">
                <h1 id="snackbar-message"></h1>
            </section>
        </section> 
    </body>
</html>
