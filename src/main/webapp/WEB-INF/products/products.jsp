<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.List"%>
<%@page import="py.com.pol.fpuna.commons.model.response.Category"%>
<%@page import="py.com.pol.fpuna.commons.entity.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Productos</title>
        <style><%@include file="/WEB-INF/products/styles.css"%></style>
        <script><%@include file="/WEB-INF/products/scripts.js"%></script>
    </head>
    <body>
        <section class="container">
            <section class="search-block">
                <section class="description-block">
                    <select id="category" onchange="searchProducts()">
                        <%
                            List<Category> categorias = (List<Category>) session.getAttribute("categorias");
                            if(categorias == null || categorias.isEmpty()){
                                out.println("<option>No hay categorías</option>");
                            }
                            else{
                                out.println("<option selected disabled value=" + -1 +">Seleccione una categoría</option>");
                                for(Category categoria : categorias){
                                    out.println("<option " + "value=" + categoria.getCategoryId()+ ">" + categoria.getDescription()+ "</option>");
                                }
                                out.println("<option value="+ 0 + ">Todas las categorías</option>");
                            }
                        %>
                    </select>
                    <input type="text" placeholder="Escriba el nombre del producto que desea encontrar" id="productName" onchange="searchProducts()" onkeyup="searchProducts()"/>
                </section>
            </section>
            <section class="product-block" id="product-block">
            <%
                List<Product> productos = (List<Product>) session.getAttribute("productos");
                if(productos == null || productos.isEmpty()){
                    out.println("<h2>No hay resultados");
                }
                else{
                    NumberFormat numberFormat = NumberFormat.getNumberInstance();
                    numberFormat.setMaximumFractionDigits(0);
                    for(Product producto : productos){              
                        out.println("<section class=" + "product-card" + ">");
                            out.println("<h2 class=" + "product-title" + ">" + producto.getDescription()+ "</h2>");
                            out.println("<h4 class=" + "product-category" + ">" + producto.getCategoryId().getDescription() + "</h3>");
                            out.println("<h3 class=" + "product-price" + "> Gs. " + numberFormat.format(producto.getPrice()) + "</h3>");
                            out.println("<button class=" + "product-buy" + " onclick=" + "addProductToKart(" + producto.getProductId() + ")" + ">Agregar</button>");
                        out.println("</section>");
                    }
                }    
            %>
            </section>
        </section>       
    </body>
</html>
