/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function searchProducts(){
    var productName = document.getElementById("productName").value;
    var categoryId = document.getElementById("category").value;    
    searchAll(isEmpty(productName) ? undefined : productName, (categoryId === "0" || categoryId === "-1") ? undefined : categoryId);
}

function searchAll(productName, categoryId){
    var xhr = new XMLHttpRequest();
    var params = "";
    if(productName !== undefined){
        params += "description=" + productName + "&";
    }
    if(categoryId !== undefined){
        params += "category=" + categoryId;
    }
    xhr.open("GET", "http://localhost:8080/parprdmcs/api/product/list?" + params, false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = xhr.responseText;
            response = JSON.parse(response);
            console.log(response);
            addProductsToDOM(response);
        }
        else if(this.status === 204){
            addProductsToDOM([]);
        }
        else{
            console.log(xhr.responseText);            
            console.log("ERROR");            
        }
    };
    xhr.send();
    console.log("searchAll");
}

function isEmpty(string){
    if(string !== undefined){
        string = string.trim();
        if(string === ""){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return true;
    }
}

function addProductsToDOM(products){
    if(products !== undefined && products.length > 0){
        console.log(products);
        var productsBlock = document.getElementById("product-block");
        removeAllChilds(productsBlock);
        for(var i=0; i < products.length; i++){
            
            console.log(products[i]);
            
            let newProduct = document.createElement("section");
            newProduct.setAttribute("class", "product-card");

            let productTitle = document.createElement("h2");
            productTitle.setAttribute("class", "product-title");
            productTitle.innerText = products[i].description;
            
            let productCategory = document.createElement("h4");
            productCategory.setAttribute("class", "product-category");
            productCategory.innerText = products[i].categoryId.description;
            
            let productPrice = document.createElement("h3");
            productPrice.setAttribute("class", "product-price");
            productPrice.innerText = new Intl.NumberFormat("de-DE").format(products[i].price) + " GS.";
            
            let buttonAdd = document.createElement("button");
            buttonAdd.setAttribute("class", "product-buy");
            buttonAdd.setAttribute("onclick", "addProductToKart(" + products[i].productId + ")");
            buttonAdd.innerText = "Comprar";
            
            newProduct.appendChild(productTitle); newProduct.appendChild(productCategory);
            newProduct.appendChild(productPrice); newProduct.appendChild(buttonAdd);
            
            productsBlock.appendChild(newProduct);                        
        }
    }
    else {
        var productsBlock = document.getElementById("product-block");
        removeAllChilds(productsBlock);        
    }
}

function getProductById(productId){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:8080/parprdmcs/api/product/" + productId, false);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function() { 
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var response = xhr.responseText;
            response = JSON.parse(response);
            console.log(response);
            addProductToDOM(response);
            return response;
        }
        else{
            console.log(xhr.responseText);
            console.log("ERROR");
        }
    };
    xhr.send();
}

function removeAllChilds(element){
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

function addProductToKart(product){
    if(!exists(product)){
        getProductById(product);        
        showSnackbar("Producto agregado al carrito", "success");
    }
    else {
        showSnackbar("El producto ya se encuentra en el carrito", "error");
    }
}

function exists(idProducto){
    var kart = document.getElementById("list-kart").value;
    if(kart === ""){
        return false;
    }
    kart = JSON.parse(kart);
    for(var i = 0; i < kart.length; i++){
        var element = JSON.parse(kart[i]);
        if(element.productId === idProducto){
            return true;
            break;
        }
    }
    return false;
}

function addProductToDOM(product){
    product.cantidadReq = 1;
    var kart = document.getElementById("list-kart").value;
    if(kart !== ""){
        kart = JSON.parse(kart);
    }
    if(!Array.isArray(kart)){
        console.log("No es array");
        kart = [];
    }
    kart.push(JSON.stringify(product));
    document.getElementById("list-kart").value = JSON.stringify(kart);
    eraseCookie("kart");
    createCookie("kart", JSON.stringify(kart), 1);
    addItemToKart(product);
}

function addItemToKart(item){
    
    console.log("addItemToKart", item);
    
    var items = document.getElementById("items");
    if(items.hasChildNodes()){
        var childNodes = items.childNodes;
        console.log("childNodes", childNodes);
        for(var i = 0; i < childNodes.length; i++){
            items.removeChild(childNodes[i]);
        }
    }   
    console.log("items dom", items);
    
    var newItem = document.createElement("section");
    newItem.setAttribute("class", "kart-item");
    
    console.log("nuevoitem", newItem);
    
    var tableElement = document.createElement("table");
    tableElement.setAttribute("class", "kart-table");
    var tableBody = document.createElement("tbody");

    var kart = JSON.parse(document.getElementById("list-kart").value);
    
    for(var i in kart){       
        
        kart[i] = JSON.parse(kart[i]);
        
        console.log("kart[" + i + "]", kart[i]);
        
        var tableRow = document.createElement("tr");

        var deleteCol = document.createElement("td");
        var deleteColImg = document.createElement("img");
        deleteColImg.setAttribute("src", "img/delete.svg");
        //deleteColImg.setAttribute("class", "btn-eliminar-item");
        deleteColImg.setAttribute("onclick", "deleteItemToKart(" + kart[i].productId + ")");
        deleteCol.appendChild(deleteColImg);
        
        var cantidadCol = document.createElement("td");
        var cantidadColInput = document.createElement("input");        
        cantidadColInput.setAttribute("min", "1");
        cantidadColInput.setAttribute("type", "number");
        cantidadColInput.setAttribute("value", kart[i].cantidadReq);
        cantidadColInput.setAttribute("class", "cant-input");
        cantidadColInput.setAttribute("id", "product-" + kart[i].productId);
        cantidadColInput.setAttribute("onchange", "addCantidad(" + kart[i].productId + ")");
        cantidadCol.appendChild(cantidadColInput);
        
        var productoCol = document.createElement("td");
        var productoColSpan = document.createElement("span");
        productoColSpan.innerText = kart[i].description;
        productoCol.appendChild(productoColSpan);
        
        var precioProductoCol = document.createElement("td");
        var precioProductoSpan = document.createElement("span");
        precioProductoCol.innerText = "Gs. " + new Intl.NumberFormat("de-DE").format(kart[i].price * kart[i].cantidadReq);
        precioProductoCol.setAttribute("class", "kart-item-text");
        precioProductoCol.setAttribute("id", "subtotal-" + kart[i].productId);
        precioProductoCol.appendChild(precioProductoSpan);
        
        tableRow.appendChild(deleteCol);
        tableRow.appendChild(cantidadCol);
        tableRow.appendChild(productoCol);
        tableRow.appendChild(precioProductoCol);
        
        tableBody.appendChild(tableRow);
    }    
    
    var tableFooter = document.createElement("tfoot");
    var tableFooterRow = document.createElement("tr");
    var tableFooterCol = document.createElement("td");
    tableFooterCol.setAttribute("style", "border-top: 1px solid white");
    tableFooterCol.setAttribute("colspan", "4");
    tableFooterCol.setAttribute("class", "kart-item-text");
    tableFooterCol.setAttribute("align", "right");
    var tableTotal = document.createElement("span");
    tableTotal.setAttribute("id", "cart-total");
    tableTotal.innerText = "Gs. " + new Intl.NumberFormat("de-DE").format(sumItems());
    tableFooterCol.appendChild(tableTotal);
    
    tableFooterRow.appendChild(tableFooterCol);
    tableFooter.appendChild(tableFooterRow);    
    
    tableElement.appendChild(tableBody);
    tableElement.appendChild(tableFooter);

    newItem.appendChild(tableElement);
    
    items.appendChild(newItem);
}

function sumItems(){
    var sum = 0;
    var kart = document.getElementById("list-kart").value;
    kart = JSON.parse(kart);
    for(var i = 0; i < kart.length; i++){
        var element = JSON.parse(kart[i]);
        console.log(element);
        sum += element.price * element.cantidadReq;
    }
    return sum;
}

function addCantidad(productId){
    var productKart = document.getElementById("product-" + productId);
    console.log(productKart);
    product = searchProductInKart(productId);
    console.log(product);    
    product.cantidadReq = parseInt(productKart.value);
    document.getElementById("subtotal-" + productId).innerText = "Gs. " + new Intl.NumberFormat("de-DE").format(product.price * product.cantidadReq);
    updateKartList(product);
}

function searchProductInKart(productId){
    var kart = document.getElementById("list-kart").value;
    kart = JSON.parse(kart);
    for(var i = 0; i < kart.length; i++){
        var element = JSON.parse(kart[i]);
        if(element.productId === productId){
            return element;
        }
    }
}

function updateKartList(product){
    var kart = document.getElementById("list-kart").value;
    kart = JSON.parse(kart);
    for(var i = 0; i < kart.length; i++){
        var element = JSON.parse(kart[i]);
        if(element.productId === product.productId){
            console.log(element);
            kart[i] = JSON.stringify(product);
            document.getElementById("list-kart").value = JSON.stringify(kart);
            document.getElementById("cart-total").innerText = "Gs. " + new Intl.NumberFormat("de-DE").format(sumItems());
            break;
        }
    }
    kart = document.getElementById("list-kart").value;
    eraseCookie("kart");
    createCookie("kart", kart, 1);
}

function deleteItemToKart(idProduct){
    var kart = document.getElementById("list-kart").value;
    kart = JSON.parse(kart);
    var newKart = [];
    for(var i = 0; i < kart.length; i++){
        var element = JSON.parse(kart[i]);
        console.log(element);
        if(element.productId !== idProduct){
            newKart.push(JSON.stringify(element));
        }
    }
    document.getElementById("list-kart").value = JSON.stringify(newKart);
    document.getElementById("cart-total").innerText = + "Gs. " + new Intl.NumberFormat("de-DE").format(sumItems());
    kart = document.getElementById("list-kart").value;
    eraseCookie("kart");
    createCookie("kart", kart, 1);
    window.location.href = "home";
}