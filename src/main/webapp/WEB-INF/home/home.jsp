<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    session.setAttribute("productos", request.getAttribute("productos"));
    session.setAttribute("categorias", request.getAttribute("categorias"));
    String currentPage = (String) request.getAttribute("page");
    Cookie[] cookieList = request.getCookies();
    Boolean existeSesion = false;
    String userId = null;
    String userEmail = null; 
    String userRol = null;
    for (int i = 0; i < cookieList.length; i++) {
        System.out.println(cookieList[i].getName()); 
        String name = cookieList[i].getName();
        if (name.equals("userId")) {
            userId = cookieList[i].getValue();
            existeSesion = true;
        }
        if (name.equals("userEmail")) {
            userEmail = cookieList[i].getValue();
            existeSesion = true;
        } 
        if (name.equals("userRol")){
            userRol = cookieList[i].getValue();
            existeSesion = true; 
        }
    } 
%> 
<!DOCTYPE html> 
<html> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>paronline | home</title>
        
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Fira+Sans+Condensed:wght@100;400;500;600;700&display=swap" rel="stylesheet">
        
        <style><%@include file="/WEB-INF/home/styles.css"%></style>
        <script><%@include file="/WEB-INF/js-core/cookie.js"%></script>
        <script><%@include file="/WEB-INF/home/scripts.js"%></script>
    </head>
    <body onload="loadResources()">
        <div class="container-home" id="container-home">  
            <header> 
                <h3><span onclick="window.location.href = 'home'" class="logo-span">paronline</span></h3>
                <%if (userRol != null && userRol.equals("1")) {%>
                    &nbsp;<a href="#" class="link-menu" id="nav-abmClientes" onclick="showABMClientes()">Clientes</a>&nbsp;
                    <a href="#" class="link-menu" id="nav-abmProductos" onclick="showABMProductos()">Productos</a>
                <%}%>
                <%
                    if (existeSesion) {
                        out.println("<section class=" + "elements-header" + ">");
                        out.println("<h3 class=" + "title-user" + ">" + userEmail + "</h3>");
                        out.println("<span class=" + "span-logout" + " onclick=" + "logout()" + ">Cerrar sesión</span>");
                        out.println("</section>");
                    }
                    else {
                        out.println("<span class=" + "span-login" + " onclick=" + "goLogin()" + ">Iniciar sesión</span>");
                    }
                %>
                <img style="margin-right: 20px; cursor: pointer;" src="img/cart.svg" onClick="showConfirmModal()"/>
            </header>
            <section class="content"> 
                <section class="page">  
                    <%if (currentPage.equals("productos")) {%> 
                    <section id="products-page"> 
                        <%@ include file="/WEB-INF/products/products.jsp"%>                        
                    </section> 
                    <%} else {%> 
                    <section id="products-page" hidden>
                        <%@ include file="/WEB-INF/products/products.jsp"%>                        
                    </section>
                    <%}%>
                    <section id="abmClientes-page" hidden>
                        <%@ include file="/WEB-INF/abm/clientes/clientes.jsp"%>
                    </section>
                    <section id="abmProductos-page" hidden>
                        <%@ include file="/WEB-INF/abm/productos/productos.jsp"%>
                    </section>
                </section>
                <input type="text" hidden id="list-kart"/>
            </section>
        </div> 
        <section id="form-clients-modal" hidden>
            <%@ include file="/WEB-INF/modals/formClient/formClient.jsp"%>
        </section>
        <section id="form-products-modal" hidden> 
            <%@ include file="/WEB-INF/modals/formProduct/formProduct.jsp"%>
        </section>
        <section id="login-modal" hidden>
            <%@ include file="/WEB-INF/login/login.jsp"%>
        </section>
        <section id="confirm-purchase-modal" hidden> 
            <%@ include file="/WEB-INF/cart/cart.jsp"%>
        </section>
        <section id="snackbar-modal" hidden>
            <%@ include file="/WEB-INF/modals/snackbar/snackbar.jsp"%>
        </section>
    </body>
</html>
